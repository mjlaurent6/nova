# Nova App 
hackUST 2022 code submission for Team mandu.

### Developer:
- Michael Jhon LAURENT
- Matthew Go DYCHENGBENG

### Stack used:
- React.js (open-source) with its packages (CRA-based initialization). 
- Material UI design (core and icons) from https://mui.com/

### Model:
This app is created for mobile device web browser. Currently, we restrict to the view only
for mobile view, if you see this on desktop, kindly reduce the window size or adjust 
to mobile view from Developer Console. 

### License and Copyrights:
- We do not own the assets (images, logo, and model) from this code except from the logic and program.
- Package used is listed on <code>package.json</code> via npm install
- Intellectual property fonts used in this project is Helvetica Neue from Google Fonts CDN.

### How to run the program:
1. Clone the program locally
2. Install dependencies, <code>--force</code> may be needed for <code>three.js</code> dependency 
    ```bash
    npm install --force
    ``` 
3. Run the program  
    ```bash
    npm run start
    ```

### How to build the program
1. Run build command
    ```bash
    npm run build
    ```

### Deployment
1. This app is deployed live on Firebase Hosting, accessible on https://nova-mvp-46aad.web.app/



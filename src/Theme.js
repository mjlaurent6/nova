import * as React from "react";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import { purple } from "@mui/material/colors";
import Button from "@mui/material/Button";

export const theme = createTheme({
  palette: {
    primary: {
      main: "#E33838",
      contrastText: "#fff",
    },
    secondary: {
      main: "#E97374",
      contrastText: "#fff",
    },
  },
});

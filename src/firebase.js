// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyBW3ZVKOWLn7ndmWmcxpYaNcPA4eGvfp6o",
  authDomain: "nova-mvp-46aad.firebaseapp.com",
  projectId: "nova-mvp-46aad",
  storageBucket: "nova-mvp-46aad.appspot.com",
  messagingSenderId: "497983290564",
  appId: "1:497983290564:web:b7b5bb2ab2d491bb20dd91",
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

export { app };

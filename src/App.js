import "./App.css";
import { useState, useEffect, useRef } from "react";
import NovaButton from "./components/NovaButton";
import AlertDialog from "./components/Alert";
import { Alert } from "@mui/material";
import { WindowOutlined } from "@mui/icons-material";
import logo from "./static/img/logo.svg";
import ChevronRightIcon from "@mui/icons-material/ChevronRight";

function getWindowDimensions() {
  const { innerWidth: width, innerHeight: height } = window;
  return {
    width,
    height,
  };
}

function App() {
  const [windowDimensions, setWindowDimensions] = useState(
    getWindowDimensions()
  );

  useEffect(() => {
    function handleResize() {
      setWindowDimensions(getWindowDimensions());
    }

    window.addEventListener("resize", handleResize);
    return () => window.removeEventListener("resize", handleResize);
  }, []);

  const messagesEndRef = useRef(null);

  const scrollToBottom = () => {
    messagesEndRef.current?.scrollIntoView({ behavior: "smooth" });
  };

  useEffect(() => {
    scrollToBottom();
  }, []);

  return (
    <div className="App">
      {windowDimensions.width > 600 ? (
        <Alert severity="error">
          This app is created for mobile, please switch to mobile view
        </Alert>
      ) : (
        <div></div>
      )}
      {windowDimensions.width > 600 ? (
        <AlertDialog />
      ) : (
        <>
          <div className="maintext">
            <img src={logo} style={{ width: 150 }} alt="logo" />
            <p>Make better shopping decisions with your digital closet</p>
          </div>
          <div className="maintext">
            <div className="getstarted">
              <NovaButton title="Get Started" icon={<ChevronRightIcon />} />
            </div>
          </div>
        </>
      )}
    </div>
  );
}

export default App;

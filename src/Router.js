import { BrowserRouter, Routes, Route } from "react-router-dom";
import {
  StartingPage,
  ExplorePage,
  MyCloset,
  ClothesPage,
  ShoppingPage,
  SearchPage,
  CatalogDetailPage,
  CatalogGridPage,
  FunRoutePage,
  ScanImagePage,
} from "./containers/index";
import React from "react";
import App from "./App";
import maps1 from "./static/img/maps1.png";
import maps2 from "./static/img/maps2.png";
import maps4 from "./static/img/maps4.png";

function Router() {
  return (
    <BrowserRouter>
      <Routes>
        {/* Add new page here */}
        <Route path="/" element={<App />} />
        <Route path="starting" exact element={<StartingPage />} />
        <Route path="explore" exact element={<ExplorePage />} />
        <Route path="/my_closet" exact element={<MyCloset />} />
        <Route path="/clothes" exact element={<ClothesPage />} />
        <Route
          path="/shopping"
          exact
          element={<ShoppingPage maps={maps1} option="true" />}
        />
        <Route
          path="/available_store"
          exact
          element={<ShoppingPage maps={maps2} option="false" />}
        />
        <Route path="/search" exact element={<SearchPage />} />
        <Route path="/my_closet_outfit" exact element={<CatalogGridPage />} />
        <Route
          path="/my_closet_detail/:id"
          exact
          element={<CatalogDetailPage />}
        />
        <Route
          path="/fun_route"
          exact
          element={
            <FunRoutePage maps={maps1} value="Try to type Causeway Bay" />
          }
        />
        <Route
          path="/fun_route/causeway_bay"
          exact
          element={<FunRoutePage maps={maps4} value="Causeway Bay" />}
        />
        <Route path="/scan_image" exact element={<ScanImagePage />} />
      </Routes>
    </BrowserRouter>
  );
}

export default Router;

import React from "react";
import { Button } from "@mui/material";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import { theme } from "../Theme";

function NovaButton({ title, icon }) {
  return (
    <ThemeProvider theme={theme}>
      <Button
        variant="contained"
        disableElevation
        color="secondary"
        endIcon={icon}
        href="/my_closet"
      >
        {title}
      </Button>
    </ThemeProvider>
  );
}

export default NovaButton;

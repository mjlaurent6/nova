import React, { useEffect, useState } from "react";
import {
  AppBar,
  IconButton,
  Toolbar,
  Grid,
  TextField,
  Typography,
} from "@mui/material";
import { Box } from "@mui/system";
import WindowIcon from "@mui/icons-material/Window";
import ShoppingCartIcon from "@mui/icons-material/ShoppingCart";
import ExploreIcon from "@mui/icons-material/Explore";
import { Search } from "@mui/icons-material";
import ButtonBase from "@mui/material/ButtonBase";
import "./BottomAppBar.css";

export default function BottomAppBar({ pages }) {
  const hoverStyle = {
    backgroundColor: `#E97374`,
    color: `#FFFFFF`,
  };

  const neutralStyle = {
    backgroundColor: `#FFFFFF`,
    color: `#B0B0B0`,
  };

  const isActive = (status) => {
    if (status) {
      return hoverStyle;
    } else {
      return neutralStyle;
    }
  };
  return (
    <React.Fragment>
      <AppBar position="fixed" color="default" sx={{ top: "auto", bottom: 0 }}>
        <Box sx={{ flexGrow: 1 }}>
          <Grid container>
            <Grid item xs={4} style={isActive(pages[0])}>
              <ButtonBase href="/my_closet">
                <WindowIcon />
                <Typography>My Closet</Typography>
              </ButtonBase>
            </Grid>
            <Grid item xs={4} style={isActive(pages[1])}>
              <ButtonBase href="/explore">
                <ExploreIcon />
                <Typography>Explore</Typography>
              </ButtonBase>
            </Grid>
            <Grid item xs={4} style={isActive(pages[2])}>
              <ButtonBase href="/shopping">
                <ShoppingCartIcon />
                <Typography>Go Shopping</Typography>
              </ButtonBase>
            </Grid>
          </Grid>
        </Box>
      </AppBar>
    </React.Fragment>
  );
}

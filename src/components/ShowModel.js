import React, { Suspense } from "react";
import { useGLTF, OrbitControls, Bounds } from "@react-three/drei";
import { Canvas } from "@react-three/fiber";

function Model({ path }) {
  const { scene } = useGLTF(path);
  return <primitive object={scene} />;
}

function ShowModel({ path }) {
  return (
    <div style={{ height: "70vh", width: "30vh", backgroundColor: "#0ffff" }}>
      <Canvas camera={{ position: [1, 1, 20], fov: 5 }}>
        <pointLight position={[1000, 1000, 1000]} intensity={1.3} />
        <pointLight position={[-1000, -1000, -1000]} intensity={1.3} />

        <Suspense fallback={null}>
          <Bounds fit clip margin={0.6}>
            <Model path={path} />
          </Bounds>
        </Suspense>
        <OrbitControls
          makeDefault
          minPolarAngle={0}
          maxPolarAngle={Math.PI / 1.75}
        />
      </Canvas>
      <h1></h1>
    </div>
  );
}

export default ShowModel;

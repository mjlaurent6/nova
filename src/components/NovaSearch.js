import React from "react";
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import "./NovaSearch.css";
import { TextField } from "@mui/material";
import { useState } from "react";
import { FrameValue } from "react-spring";

function NovaSearch({ handleSubmit, value }) {
  const [name, changeName] = useState(value);
  const handleEnter = (e) => {
    e.preventDefault();
    window.location.href = "/fun_route/causeway_bay";
  };
  return (
    <div className="search">
      <Box sx={{ flexGrow: 1 }}>
        <Grid container columnSpacing={{ md: 0 }}>
          <Grid item xs={12}>
            {handleSubmit === "true" ? (
              <form onSubmit={handleEnter}>
                <TextField
                  onChange={(e) => changeName(e.target.value)}
                  value={name}
                  focused
                  fullWidth
                  placeholder="Search"
                />
              </form>
            ) : (
              <TextField
                onChange={(e) => changeName(e.target.value)}
                value={name}
                focused
                fullWidth
                placeholder="Search"
              />
            )}
          </Grid>
        </Grid>
      </Box>
    </div>
  );
}

export default NovaSearch;

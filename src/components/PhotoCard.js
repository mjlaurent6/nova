import React from "react";
import "./PhotoCard.css";
import { Link } from "react-router-dom";
function PhotoCard(props) {
  const styles = {
    backgroundImage: `url(${props.image})`,
    backgroundRepeat: "no-repeat",
  };
  return (
    <Link to="/clothes">
      <div className="container" style={styles}></div>
    </Link>
  );
}

export default PhotoCard;

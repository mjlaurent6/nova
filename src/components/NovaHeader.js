import "./NovaHeader.css";
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import { Link, useNavigate } from "react-router-dom";

export default function NovaHeader({ left, right, leftLink, rightLink }) {
  const navigate = useNavigate();
  return (
    <Box sx={{ flexGrow: 1 }}>
      <Grid container columnSpacing={{ md: 0 }}>
        <Grid item xs={12}>
          <div className="topbar">
            {left ? (
              <div className="left">
                <div onClick={() => navigate(-1)} className="back">
                  {left}
                </div>
              </div>
            ) : (
              <></>
            )}
            <div className="center">
              <h2>NOVA</h2>
            </div>
            {right ? (
              <div className="right">
                <Link to={rightLink}>
                  <div className="back">{right}</div>
                </Link>
              </div>
            ) : (
              <></>
            )}
          </div>
        </Grid>
      </Grid>
    </Box>
  );
}

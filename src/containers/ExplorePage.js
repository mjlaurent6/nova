import React from "react";
import PhotoCard from "../components/PhotoCard";
import "./ExplorePage.css";
import photo1 from "../static/img/explore1.png";
import photo2 from "../static/img/explore2.png";
import photo3 from "../static/img/explore3.png";
import photo4 from "../static/img/explore4.png";
import photo5 from "../static/img/explore5.png";
import photo6 from "../static/img/explore6.png";
import SearchIcon from "@mui/icons-material/Search";
import Card from "@mui/material/Card";
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import Container from "@mui/material/Container";
import CardMedia from "@mui/material/CardMedia";
import FavoriteIcon from "@mui/icons-material/Favorite";
import AccountCircleIcon from "@mui/icons-material/AccountCircle";
import { styled } from "@mui/system";
import { TextField } from "@mui/material";
import BottomAppBar from "../components/BottomAppBar";
import { Link } from "react-router-dom";
import NovaSearch from "../components/NovaSearch";
import ArrowBackIosNewIcon from "@mui/icons-material/ArrowBackIosNew";
import NovaHeader from "../components/NovaHeader";

function ImageCard({ route, image }) {
  return (
    <Grid item xs={6}>
      <Link to={route}>
        <div className="explore_image">
          <Container maxWidth="md">
            <Card sx={{ maxWidth: window.innerWidth / 2 }}>
              <CardMedia component="img" image={image} alt="" />
            </Card>
          </Container>
        </div>
      </Link>
    </Grid>
  );
}

function ExplorePage() {
  return (
    <React.Fragment>
      <div className="container">
        <NovaHeader
          left={<ArrowBackIosNewIcon />}
          right={<SearchIcon />}
          center="NOVA"
          leftLink="/explore"
          rightLink="/search"
        />
        <NovaSearch />
        <Box sx={{ flexGrow: 1 }}>
          <Grid container rowSpacing={2} columnSpacing={4}>
            <ImageCard route="/clothes" image={photo1} />
            <ImageCard route="/clothes" image={photo2} />
            <ImageCard route="/clothes" image={photo3} />
            <ImageCard route="/clothes" image={photo4} />
            <ImageCard route="/clothes" image={photo5} />
            <ImageCard route="/clothes" image={photo6} />
          </Grid>
        </Box>
      </div>
      <BottomAppBar pages={[false, true, false]} />
    </React.Fragment>
  );
}

export default ExplorePage;

import React from "react";
import BottomAppBar from "../components/BottomAppBar";
import ShowModel from "../components/ShowModel";
import AccessibilityIcon from "@mui/icons-material/Accessibility";
import RestartAltIcon from "@mui/icons-material/RestartAlt";
import LightbulbIcon from "@mui/icons-material/LightbulbOutlined";
import "./MyCloset.css";
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import styled from "styled-components";
import icon from "../static/img/plus_icon.png";
import { useState } from "react";
import shirtOne from "../static/img/shirtOne.svg";
import shirtTwo from "../static/img/shirtTwo.svg";
import shirtThree from "../static/img/shirtThree.svg";
import shirtFour from "../static/img/shirtFour.svg";
import shirtFive from "../static/img/shirtFive.svg";
import shirtFourToggle from "../static/img/shirtFourNew.svg";
import NovaHeader from "../components/NovaHeader";
import ArrowBackIosNewIcon from "@mui/icons-material/ArrowBackIosNew";
import SearchIcon from "@mui/icons-material/Search";

const clothes = [
  { image: shirtOne },
  { image: shirtTwo },
  { image: shirtThree },
  { image: shirtFive },
];

function MyCloset() {
  const [data, setData] = useState({
    path: "whiteturtle.glb",
    image: shirtFour,
  });
  const handleClick = () => {
    const temp = { path: "hoodie.glb", image: shirtFourToggle };
    setData(temp);
  };
  const handleNeutral = () => {
    const temp = { path: "whiteturtle.glb", image: shirtFour };
    setData(temp);
  };
  return (
    <div>
      <div className="master_container">
        <NovaHeader
          left={<ArrowBackIosNewIcon />}
          right={<SearchIcon />}
          center="NOVA"
          leftLink="/explore"
          rightLink="/search"
        />
      </div>
      <div
        style={{
          backgroundImage: "url(/bg.png)",
          height: "100vh",
          backgroundPosition: "center",
          backgroundRepeat: "no-repeat",
          backgroundSize: "cover",
        }}
      >
        <div className="my_closet_container">
          <div className="model" onClick={handleNeutral}>
            <RestartAltIcon />
            Reset Look
            <ShowModel path={data.path} />
          </div>
          <div className="closet">
            <Box className="box_overlay" sx={{ left: "auto", right: 0 }}>
              <div className="closet_grid">
                <div className="sidescreentop">My Closet</div>
                <br />
                {clothes ? (
                  clothes.map((value, key) => (
                    <div
                      className="img_container"
                      onClick={handleNeutral}
                      key={key}
                    >
                      <img
                        alt="text"
                        className="img_closet"
                        src={value.image}
                      />
                    </div>
                  ))
                ) : (
                  <></>
                )}
                <div className="img_container" onClick={handleClick}>
                  <img alt="text" className="img_closet" src={data.image} />
                </div>
                <div className="generate">
                  <LightbulbIcon />
                  Generate OOTD
                </div>
              </div>
            </Box>
          </div>
        </div>
        <Box sx={{ top: "auto" }}>
          <Grid container columnSpacing={{ md: 0 }}>
            <Grid item xs={12}>
              <div className="bottomicon">
                <div style={{ color: "white" }}>
                  <AccessibilityIcon />
                  Edit my avatar
                </div>
                <div className="name"></div>
                <div className="share"></div>
                <div className="comment"></div>
                <div className="like"></div>
                <div className="like"></div>
                <img src={icon} alt="text" />
              </div>
            </Grid>
          </Grid>
        </Box>
      </div>
      <React.Fragment>
        <BottomAppBar pages={[true, false, false]} />
      </React.Fragment>
    </div>
  );
}

export default MyCloset;

import React from "react";
import NovaHeader from "../components/NovaHeader";
import ArrowBackIosNewIcon from "@mui/icons-material/ArrowBackIosNew";
import ShareIcon from "@mui/icons-material/Share";
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import scan from "../static/img/scan_area.png";
import ImageIcon from "@mui/icons-material/Image";
import "./ScanImagePage.css";
function ScanImagePage() {
  return (
    <div className="scan_image">
      <NovaHeader
        left={<ArrowBackIosNewIcon />}
        right={<ShareIcon />}
        center="NOVA"
        leftLink="/"
        rightLink="/shopping"
      />
      <div className="scan_text">Please Scan the Barcode or QR code</div>
      <Box sx={{ flexGrow: 1 }}>
        <Grid container columnSpacing={{ md: 0 }}>
          <Grid item xs={12}>
            <a href="/my_closet_detail/photo1">
              <img className="fullimage" src={scan} alt="l" />
            </a>
          </Grid>
        </Grid>
        <div className="scan_bottom">
          <ImageIcon />
          <div className="scan_text">Choose a picture</div>
        </div>
      </Box>
    </div>
  );
}

export default ScanImagePage;

import React from "react";
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import Container from "@mui/material/Container";
import { TextField } from "@mui/material";
import "./ClothesPage.css";
import ArrowBackIosNewIcon from "@mui/icons-material/ArrowBackIosNew";
import image from "../static/img/clothes1.png";
import AccountCircleIcon from "@mui/icons-material/AccountCircle";
import ShareIcon from "@mui/icons-material/Share";
import ChatBubbleOutlineIcon from "@mui/icons-material/ChatBubbleOutline";
import FavoriteIcon from "@mui/icons-material/Favorite";
import { AppBar } from "@mui/material";
import { Link } from "react-router-dom";
import PopUp from "../components/PopUp";
import { Card } from "@mui/material";
import CardMedia from "@mui/material/CardMedia";
import popup from "../static/img/popup.png";
import { CardContent } from "@mui/material";
import hm from "../static/img/hm.svg";
import NovaButton from "../components/NovaButton";
import NovaHeader from "../components/NovaHeader";
function ImageCard({ image }) {
  return (
    <Card>
      <CardContent>
        <div className="header">
          <img src={hm} alt="t" />
          <p>Cropped hooded top</p>
        </div>
      </CardContent>
      <CardMedia component="img" image={image} alt="" />
      <div className="footer">
        <NovaButton title="See it on me" />
      </div>
      <br />
    </Card>
  );
}

function ClothesPage() {
  return (
    <div className="clothes_container">
      <NovaHeader
        left={<ArrowBackIosNewIcon />}
        right={<ShareIcon />}
        center="NOVA"
        leftLink="/explore"
        rightLink="/search"
      />
      <PopUp popup={<ImageCard image={popup} />}>
        <Box sx={{ flexGrow: 1 }}>
          <Grid container columnSpacing={{ md: 0 }}>
            <Grid item xs={12}>
              <img className="fullimage" src={image} alt="l" />
            </Grid>
          </Grid>
        </Box>
      </PopUp>
      <AppBar position="fixed" color="default" sx={{ top: "auto", bottom: 0 }}>
        <Box sx={{ top: "auto", bottom: 0 }}>
          <Grid container columnSpacing={{ md: 0 }}>
            <Grid item xs={12}>
              <div className="bottombar">
                <div className="icon">
                  <AccountCircleIcon />
                </div>
                <div className="name">Isabella Diaz</div>
                <div className="share">
                  <ShareIcon />
                </div>
                <div className="comment">
                  <ChatBubbleOutlineIcon />
                </div>
                <div className="like">
                  <FavoriteIcon />
                </div>
              </div>
            </Grid>
          </Grid>
        </Box>
      </AppBar>
    </div>
  );
}

export default ClothesPage;

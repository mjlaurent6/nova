import React from "react";
import NovaHeader from "../components/NovaHeader";
import NovaSearch from "../components/NovaSearch";
import ArrowBackIosNewIcon from "@mui/icons-material/ArrowBackIosNew";
import QrCodeScannerIcon from "@mui/icons-material/QrCodeScanner";
import Card from "@mui/material/Card";
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import Container from "@mui/material/Container";
import CardMedia from "@mui/material/CardMedia";
import { Link, useParams } from "react-router-dom";
import photo1 from "../static/img/hoodie1.png";
import photo2 from "../static/img/hoodie2.png";
import photo3 from "../static/img/hoodie3.png";
import photo4 from "../static/img/hoodie4.png";
import photo5 from "../static/img/hoodie5.png";
import photo6 from "../static/img/hoodie6.png";
import ShareIcon from "@mui/icons-material/Share";

import "./CatalogGridPage.css";
import "./CatalogDetailPage.css";

import hm from "../static/img/hm.svg";

const data = [
  { image: photo1, name: "Relaxed fit hoodie", price: "HKD150" },
  { image: photo2, name: "Cotton Sweatshirt", price: "HKD150" },
  { image: photo3, name: "Oversized fit Cotton Hoodie", price: "HKD200" },
  { image: photo4, name: "Regular Fit Reflective Hoodie", price: "HKD250" },
  { image: photo5, name: "Oversized fit Cotton Hoodie", price: "HKD200" },
  { image: photo6, name: "Regular Fit Reflective Hoodie", price: "HKD250" },
];

function ImageCard({ image, name, price }) {
  return (
    <Grid item xs={6}>
      <div className="explore_image">
        <Container maxWidth="md">
          <Card sx={{ maxWidth: window.innerWidth / 2 }}>
            <Link to="/my_closet_detail/photo1">
              <CardMedia component="img" image={image} alt="" />
            </Link>
          </Card>
          <div className="product_details">
            <img src={hm} alt="t" />
            <div className="desc">
              <div className="name">{name}</div>
              <div className="price">{price}</div>
            </div>
          </div>
        </Container>
      </div>
    </Grid>
  );
}

function CatalogDetailPage() {
  const { id } = useParams();
  let focus = { photo: photo1, name: data[0].name, price: data[0].price };
  if (id === "photo1") {
    focus.photo = photo1;
    focus.name = data[0].name;
    focus.price = data[0].price;
  } else if (id === "photo2") {
    focus.photo = photo2;
    focus.name = data[1].name;
    focus.price = data[1].price;
  } else if (id === "photo3") {
    focus.photo = photo3;
    focus.name = data[2].name;
    focus.price = data[2].price;
  } else if (id === "photo4") {
    focus.photo = photo4;
    focus.name = data[3].name;
    focus.price = data[3].price;
  } else if (id === "photo5") {
    focus.photo = photo5;
    focus.name = data[4].name;
    focus.price = data[4].price;
  } else if (id === "photo6") {
    focus.photo = photo6;
    focus.name = data[5].name;
    focus.price = data[5].price;
  }

  return (
    <div className="gridpage">
      <NovaHeader
        left={<ArrowBackIosNewIcon />}
        right={<ShareIcon />}
        center="NOVA"
        leftLink="/"
        rightLink="/shopping"
      />
      <Box sx={{ flexGrow: 1 }}>
        <Grid container columnSpacing={{ md: 0 }}>
          <Grid item xs={12}>
            <img className="fullimage" src={focus.photo} alt="l" />
          </Grid>
        </Grid>
        <div className="product_details">
          <img src={hm} alt="t" />
          <div className="desc">
            <div className="name_highligth">{focus.name}</div>
            <div className="price">{focus.price}</div>
          </div>
        </div>
      </Box>
      <div className="action_button">
        <a href="/available_store" className="find_stores">
          Find stores
        </a>
        <a href="/my_closet" className="see_on_me">
          See it on me
        </a>
      </div>
      <Box sx={{ flexGrow: 1 }}>
        <div className="similar">Similar items</div>
        <Grid container columnSpacing={5} rowSpacing={1}>
          {data ? (
            data.map((item, key) => (
              <ImageCard
                key={key}
                image={item.image}
                name={item.name}
                price={item.price}
              />
            ))
          ) : (
            <></>
          )}
        </Grid>
      </Box>
    </div>
  );
}

export default CatalogDetailPage;

import React from "react";
import NovaHeader from "../components/NovaHeader";
import NovaSearch from "../components/NovaSearch";
import ArrowBackIosNewIcon from "@mui/icons-material/ArrowBackIosNew";
import QrCodeScannerIcon from "@mui/icons-material/QrCodeScanner";
import "./SearchPage.css";
import { Link } from "react-router-dom";
import ShareIcon from "@mui/icons-material/Share";

const brandData = [
  "HnM",
  "Burberry",
  "Zara",
  "Uniqlo",
  "Stadivarius",
  "Rip Curl",
];

const greenBrands = [
  "Basics for Basics",
  "Cosmos Studios",
  "Paper Shades",
  "The Hula",
];
const products = [
  "T-shirts & Tanks",
  "Hoodies & Sweatshirts",
  "Shirts",
  "Trousers",
  "Jeans",
  "Shorts",
  "Sportswear",
];
const events = ["Anniversary", "Clubbing", "Birthday", "Sports", "Cinema"];
function Filter({ filter, data, color }) {
  return (
    <>
      {color === "green" ? (
        <div className="category">
          <div className="type green">{filter}</div>
          <div className="item">
            {data ? (
              data.map((data, key) => (
                <div className="item_each green_border" key={key}>
                  {data}
                </div>
              ))
            ) : (
              <div className="item_each green_border">No option available</div>
            )}
          </div>
        </div>
      ) : (
        <div className="category">
          <div className="type">{filter}</div>
          <div className="item">
            {data ? (
              data.map((data, key) => (
                <a href="/my_closet_outfit" className="item_each">
                  {data}
                </a>
              ))
            ) : (
              <div className="item_each">No option available</div>
            )}
          </div>
        </div>
      )}
    </>
  );
}

function SearchPage() {
  return (
    <React.Fragment>
      <div className="search_container">
        <NovaHeader
          left={<ArrowBackIosNewIcon />}
          right={<ShareIcon />}
          center="NOVA"
          leftLink="/"
          rightLink="/shopping"
        />
        <NovaSearch />
        <Filter filter="Brands" data={brandData} />
        <Filter
          filter="Green Brands Near You"
          data={greenBrands}
          color="green"
        />
        <Filter filter="Products" data={products} />
        <Filter filter="Events" data={events} />
      </div>
    </React.Fragment>
  );
}

export default SearchPage;

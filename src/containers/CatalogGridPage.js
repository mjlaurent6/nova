import React from "react";
import NovaHeader from "../components/NovaHeader";
import NovaSearch from "../components/NovaSearch";
import ArrowBackIosNewIcon from "@mui/icons-material/ArrowBackIosNew";
import QrCodeScannerIcon from "@mui/icons-material/QrCodeScanner";
import Card from "@mui/material/Card";
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import Container from "@mui/material/Container";
import CardMedia from "@mui/material/CardMedia";
import { Link, useParams } from "react-router-dom";
import photo1 from "../static/img/hoodie1.png";
import photo2 from "../static/img/hoodie2.png";
import photo3 from "../static/img/hoodie3.png";
import photo4 from "../static/img/hoodie4.png";
import photo5 from "../static/img/hoodie5.png";
import photo6 from "../static/img/hoodie6.png";
import "./CatalogGridPage.css";
import hm from "../static/img/hm.svg";
import ShareIcon from "@mui/icons-material/Share";

const data = [
  {
    image: photo1,
    name: "Relaxed fit hoodie",
    price: "HKD150",
    link: "photo1",
  },
  { image: photo2, name: "Cotton Sweatshirt", price: "HKD150", link: "photo2" },
  {
    image: photo3,
    name: "Oversized fit Cotton Hoodie",
    price: "HKD200",
    link: "photo3",
  },
  {
    image: photo4,
    name: "Regular Fit Reflective Hoodie",
    price: "HKD250",
    link: "photo4",
  },
  {
    image: photo5,
    name: "Oversized fit Cotton Hoodie",
    price: "HKD200",
    link: "photo5",
  },
  {
    image: photo6,
    name: "Regular Fit Reflective Hoodie",
    price: "HKD250",
    link: "photo6",
  },
];

function ImageCard({ image, name, price, link }) {
  let redirect = "/my_closet_detail/" + link;
  return (
    <Grid item xs={6}>
      <div className="explore_image">
        <Container maxWidth="md">
          <Card sx={{ maxWidth: window.innerWidth / 2 }}>
            <Link to={redirect}>
              <CardMedia component="img" image={image} alt="" />
            </Link>
          </Card>
          <div className="product_details">
            <img src={hm} alt="t" />
            <div className="desc">
              <div className="name">{name}</div>
              <div className="price">{price}</div>
            </div>
          </div>
        </Container>
      </div>
    </Grid>
  );
}

function CatalogGridPage() {
  return (
    <div className="gridpage">
      <NovaHeader
        left={<ArrowBackIosNewIcon />}
        right={<ShareIcon />}
        center="NOVA"
        leftLink="/"
        rightLink="/shopping"
      />
      <NovaSearch />
      <Box sx={{ flexGrow: 1 }}>
        <Grid container columnSpacing={5} rowSpacing={1}>
          {data ? (
            data.map((item, key) => (
              <ImageCard
                key={key}
                link={item.link}
                image={item.image}
                name={item.name}
                price={item.price}
              />
            ))
          ) : (
            <></>
          )}
        </Grid>
      </Box>
    </div>
  );
}

export default CatalogGridPage;

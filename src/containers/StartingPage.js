import React from "react";
import NovaButton from "../components/NovaButton";
import "./StartingPage.css";

function StartingPage() {
  return (
    <div className="app">
      <h1>this is starting page</h1>
      <NovaButton />
    </div>
  );
}

export default StartingPage;

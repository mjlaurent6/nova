import StartingPage from "./StartingPage";
import ExplorePage from "./ExplorePage";
import MyCloset from "./MyCloset";
import ClothesPage from "./ClothesPage";
import CatalogDetailPage from "./CatalogDetailPage";
import CatalogGridPage from "./CatalogGridPage";
import FindStorePage from "./FindStorePage";
import SearchPage from "./SearchPage";
import ShoppingPage from "./ShoppingPage";
import FunRoutePage from "./FunRoutePage";
import ScanImagePage from "./ScanImagePage";
export {
  StartingPage,
  ExplorePage,
  MyCloset,
  ClothesPage,
  CatalogDetailPage,
  CatalogGridPage,
  FindStorePage,
  SearchPage,
  ShoppingPage,
  FunRoutePage,
  ScanImagePage,
};

import React from "react";
import NovaHeader from "../components/NovaHeader";
import ArrowBackIosNewIcon from "@mui/icons-material/ArrowBackIosNew";
import QrCodeScannerIcon from "@mui/icons-material/QrCodeScanner";
import "./FunRoutePage.css";
import BottomAppBar from "../components/BottomAppBar";
import NovaSearch from "../components/NovaSearch";
import SearchIcon from "@mui/icons-material/Search";

function FunRoutePage({ maps, value }) {
  return (
    <React.Fragment>
      <div className="fun_route">
        <NovaHeader
          left={<ArrowBackIosNewIcon />}
          right={<SearchIcon />}
          center="NOVA"
          leftLink="/"
          rightLink="/search"
        />
        <NovaSearch handleSubmit="true" value={value} />
      </div>
      <img className="maps" src={maps} alt="text" />
      <BottomAppBar pages={[false, false, true]} />
    </React.Fragment>
  );
}

export default FunRoutePage;

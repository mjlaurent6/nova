import React from "react";
import NovaHeader from "../components/NovaHeader";
import ArrowBackIosNewIcon from "@mui/icons-material/ArrowBackIosNew";
import QrCodeScannerIcon from "@mui/icons-material/QrCodeScanner";
import "./ShoppingPage.css";
import BottomAppBar from "../components/BottomAppBar";

function ShoppingPage({ maps, option }) {
  return (
    <React.Fragment>
      <div className="shopping">
        <NovaHeader
          left={<ArrowBackIosNewIcon />}
          right={<QrCodeScannerIcon />}
          center="NOVA"
          leftLink="/"
          rightLink="/scan_image"
        />
        {option === "true" ? (
          <div className="choice">
            <a href="/search" className="leftchoice">
              I have an item in mind
            </a>
            <a href="/fun_route" className="rightchoice">
              Give me a fun route
            </a>
          </div>
        ) : (
          <></>
        )}
      </div>
      <img className="maps" src={maps} alt="text" />
      <BottomAppBar pages={[false, false, true]} />
    </React.Fragment>
  );
}

export default ShoppingPage;
